# Acerca de mí
¡Hola! 

Soy Andree, estoy tomando la ruta de aprendizaje 'HTML y CSS a Profundidad', en [platzi.com](https://platzi.com/),
actualmente soy Desarrollador Front End, y me agrada seguir aprendiento.

Trabajemos juntos, te dejo por acá mi contacto.
```
📩 hola@andreemalerva.com
📲 +52 2283530727
```

# Acerca del proyecto

Este es un proyecto del 'Curso Práctico de Frontend Developer', cada commit representa una clase del curso mencionado.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://platzi.com/)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA

